package Model.Document;

import Model.TradingPartnerName;

public class BillStrategy implements DocumentStrategy {

    private TradingPartnerName client;
    private String address;
    private String nip;

    public BillStrategy(TradingPartnerName client, String address, String nip) {
        this.client = client;
        this.address = address;
        this.nip = nip;
    }

    @Override
    public String getPartnerName() {
        return client.getName();
    }


    @Override
    public String getDocumentTypeString() {
        return "Faktura";
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }
}
