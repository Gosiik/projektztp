package Model.Document;

import Model.TradingPartnerName;

public class ReceiptStrategy implements DocumentStrategy {

    @Override
    public String getPartnerName() {
        return "Klient detaliczny";
    }

    @Override
    public String getDocumentTypeString() {
        return "Paragon";
    }

    @Override
    public String getAddress() {
        return "Brak";
    }

    @Override
    public String getNip() {
        return "Brak";
    }

}
