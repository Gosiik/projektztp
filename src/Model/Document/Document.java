package Model.Document;

import Model.Order.IOrder;

import java.time.LocalDate;

public class Document{

    private IOrder order;
    private LocalDate date;
    private DocumentStrategy documentStrategy;

    public Document(IOrder order, DocumentStrategy documentStrategy, LocalDate date) {
        this.order = order;
        this.documentStrategy = documentStrategy;
        this.date = date;
    }

    public IOrder getOrder() {
        return order;
    }

    public String getDateString(){ return date.toString(); }
    public String getPartnerName(){
        return documentStrategy.getPartnerName();
    }

    public DocumentStrategy getDocumentStrategy() {
        return documentStrategy;
    }

    public String getDocumentTypeString(){
        return documentStrategy.getDocumentTypeString();
    }

    public LocalDate getDate() {
        return date;
    }

    public String getNip(){return documentStrategy.getNip();}
    public String getAddress(){return documentStrategy.getAddress();}
}
