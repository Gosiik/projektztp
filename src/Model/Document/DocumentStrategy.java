package Model.Document;

public interface DocumentStrategy {

    public String getPartnerName();
    public String getDocumentTypeString();
    public String getAddress();
    public String getNip();

}
