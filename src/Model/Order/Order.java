package Model.Order;

import Model.Product.Product;
import Model.TradingPartnerName;

import java.util.List;

public class Order implements IOrder {

    int orderID;
    protected List<Product> products;
    private TradingPartnerName partner;
    private Double sumPrice;



    public Order(){

    }

    public Order(List<Product> products, TradingPartnerName partner) {
        this.products = products;
        this.partner = partner;
    }

    public String getPartner() {
        return partner.getName();
    }

    public void setPartner(TradingPartnerName partner) {
        this.partner = partner;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public TradingPartnerName getPartnerObject(){
        return partner;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public double getSumNetPrice(){
        double sum = 0;

        for (Product p: products) {
            sum+=p.getSumNetPrice();
        }

        return sum;
    }

    public double getSumGrossPrice(){
        double sum = 0;

        for (Product p: products) {
            sum+=p.getSumGrossPrice();
        }

        return sum;
    }

}
