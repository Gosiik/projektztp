package Model.Order;

public class OrderWithBigDiscount extends OrderDecorator {

    public OrderWithBigDiscount(IOrder iOrder) {
        super(iOrder);
    }

    @Override
    public double getSumNetPrice() {
        return super.getSumNetPrice()*0.80;
    }

    @Override
    public double getSumGrossPrice() {
        return iOrder.getSumGrossPrice()*0.80;
    }
}
