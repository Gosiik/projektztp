package Model.Order;

import Model.Product.Product;
import Model.TradingPartnerName;

import java.util.List;

public interface IOrder {

    public String getPartner();

    public void setPartner(TradingPartnerName partner);

    public List<Product> getProducts();

    public void setProducts(List<Product> products);

    public TradingPartnerName getPartnerObject();

    public double getSumNetPrice();

    public double getSumGrossPrice();

    public int getOrderID();

    public void setOrderID(int orderID);
}
