package Model.Order;

import Model.Product.Product;
import Model.TradingPartnerName;

import java.util.List;

public class OrderDecorator implements IOrder{

    protected IOrder iOrder;

    public OrderDecorator(IOrder iOrder){
        this.iOrder=iOrder;
    }


    @Override
    public String getPartner() {
        return iOrder.getPartner();
    }

    @Override
    public void setPartner(TradingPartnerName partner) {
        iOrder.setPartner(partner);
    }

    @Override
    public List<Product> getProducts() {
        return iOrder.getProducts();
    }

    @Override
    public void setProducts(List<Product> products) {
        iOrder.setProducts(products);
    }

    @Override
    public TradingPartnerName getPartnerObject() {
        return iOrder.getPartnerObject();
    }

    @Override
    public double getSumNetPrice() {
        return iOrder.getSumNetPrice();
    }

    @Override
    public double getSumGrossPrice() {
        return iOrder.getSumGrossPrice();
    }

    @Override
    public int getOrderID(){ return iOrder.getOrderID(); }

    @Override
    public void setOrderID(int orderID) {
        iOrder.setOrderID(orderID);
    }

    ;


}
