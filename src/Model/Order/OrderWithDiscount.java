package Model.Order;

public class OrderWithDiscount extends OrderDecorator{


    public OrderWithDiscount(IOrder iOrder) {
        super(iOrder);
    }

    @Override
    public double getSumNetPrice() {
        return super.getSumNetPrice()*0.95;
    }

    @Override
    public double getSumGrossPrice() {
        return iOrder.getSumGrossPrice()*0.95;
    }
}
