package Model.Raport;

import Model.Product.Product;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.NumberFormat;
import java.time.LocalDate;

public class PDFBuilder implements FinanseReportBuilder {

    private com.itextpdf.text.Document pdfDocument;
    private PdfPTable table;
    private PdfWriter writer;

    public PDFBuilder(){
        pdfDocument = new com.itextpdf.text.Document();
        try{
            String filename = "Raporty\\Raport_" + LocalDate.now() + ".pdf";
            writer = PdfWriter.getInstance(pdfDocument, new FileOutputStream(filename));
            pdfDocument.open();
        }
        catch (FileNotFoundException ex){
            ex.printStackTrace();
        }
        catch (DocumentException ex){
            ex.printStackTrace();
        }

    }

    public void saveFile(){
        pdfDocument.close();
        writer.close();
    }

    public void endTable() {
        try {
            pdfDocument.add(table);
        }catch (DocumentException ex) {
            System.out.println("There is problem with adding data to document or in closing pdfDocument");
            ex.printStackTrace();
        }

    }


    public void addStringToFile(String string){
        try {
            pdfDocument.add(new Paragraph(string));
        }catch (DocumentException ex){
            System.out.println("There is problem with adding a paragraph into pdf file");
            ex.printStackTrace();
        }
    }

    public void addTableHeaders(String[] headers){
        table = new PdfPTable(headers.length);
        table.setSpacingBefore(10f);
        table.setSpacingAfter(10f);

        for(String header : headers){
            PdfPCell cell = new PdfPCell(new Paragraph(header));
            table.addCell(cell);
        }
    }

    public void addDataToRow(Product product){
        PdfPCell name = new PdfPCell(new Paragraph(product.getName()));

        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(3);
        String amountString = nf.format(product.getAmount());
        String priceGrossString = nf.format(product.getGrossPrice());
        String priceNetString = nf.format(product.getNetPrice());

        PdfPCell amount = new PdfPCell(new Paragraph(amountString));
        PdfPCell netPrice = new PdfPCell(new Paragraph(priceGrossString));
        PdfPCell GrossPrice = new PdfPCell(new Paragraph(priceNetString));

        table.addCell(name);
        table.addCell(amount);
        table.addCell(netPrice);
        table.addCell(GrossPrice);

    }

}
