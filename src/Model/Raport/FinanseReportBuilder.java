package Model.Raport;

import Model.Product.Product;

public interface FinanseReportBuilder {

    void addTableHeaders(String[] headers);
    void addDataToRow(Product product);
    void addStringToFile(String string);
    void saveFile();
    void endTable();

}
