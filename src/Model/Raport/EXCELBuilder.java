package Model.Raport;

import Model.Product.Product;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;

public class EXCELBuilder implements FinanseReportBuilder {


    private Sheet sh;
    private int actuallyRow;
    private Workbook workbook;

    public EXCELBuilder() {
        workbook = new XSSFWorkbook();
        sh = workbook.createSheet("Raport");
        actuallyRow = 0;
    }

    public void saveFile(){
        try {
            String filename = "Raporty\\Raport_" + LocalDate.now() + ".xlsx";
            FileOutputStream fos = new FileOutputStream(filename);
            workbook.write(fos);
            fos.close();
        }catch (FileNotFoundException ex){
            System.out.println("There was problem with opening/creating file");
            ex.printStackTrace();
        }catch (IOException ex){
            System.out.println("There was problem with closing a file");
            ex.printStackTrace();
        }
    }


    public void endTable() {
        actuallyRow++;
    }

    public void addStringToFile(String string){
        sh.createRow(actuallyRow);
        sh.getRow(actuallyRow).createCell(0).setCellValue(string);
        actuallyRow++;
    }


    public void addTableHeaders(String[] headers){
        sh.createRow(actuallyRow);
        int i = 0;
        for(String header : headers){
            sh.getRow(actuallyRow).createCell(i).setCellValue(header);
            i++;
        }
        actuallyRow++;
    }

    public void addDataToRow(Product product){
        sh.createRow(actuallyRow);
        sh.getRow(actuallyRow).createCell(0).setCellValue(product.getName());
        sh.getRow(actuallyRow).createCell(1).setCellValue(product.getAmount());
        sh.getRow(actuallyRow).createCell(2).setCellValue(product.getGrossPrice());
        sh.getRow(actuallyRow).createCell(3).setCellValue(product.getNetPrice());
        actuallyRow++;
    }
}
