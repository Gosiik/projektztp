package Model.Raport;

import DB.DataBase;
import DB.VirtualDataBase;
import Model.Document.Document;
import Model.Product.Product;

import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DirectorRaport {

    public FinanseReportBuilder raportBuilder;
    public LocalDate raportDate = LocalDate.now();
    public List<Document> documents;

    public DirectorRaport(FinanseReportBuilder raportBuilder) {
        this.raportBuilder = raportBuilder;
        documents = VirtualDataBase.getInstance().getDocuments();

        // wyciagniecie podlisty zawierajacej dokumenty tylko z aktualnego dnia
        Stream stream = VirtualDataBase.getInstance()
                .getDocuments()
                .stream()
                .filter(document -> document.getDateString().equals(raportDate.toString()));

        documents = (List<Document>) stream.collect(Collectors.toList());

    }


    // Funkcja konstrułuje plik zawierający raport z aktualnego dnia i zapisuje go do folder Raporty
    public void construct() {
        if (documents != null && documents.size() != 0) {
            for (Document document : documents) {
                raportBuilder.addStringToFile("Klient: " + document.getPartnerName());
                raportBuilder.addStringToFile("Data: " + LocalDate.now());
                raportBuilder.addStringToFile("Typ dokumentu: " + document.getDocumentTypeString());
                raportBuilder.addStringToFile("NIP: " + document.getNip());
                raportBuilder.addStringToFile("Adres: " + document.getAddress());

                NumberFormat nf = NumberFormat.getInstance();
                nf.setMaximumFractionDigits(3);
                String sumGrossPrice = nf.format(document.getOrder().getSumGrossPrice());
                String sumNetPrice = nf.format(document.getOrder().getSumNetPrice());
                raportBuilder.addStringToFile("Suma do zaplaty: Brutto: " + sumGrossPrice + " Netto: " + sumNetPrice);

                raportBuilder.addStringToFile("Produkty:");
                String[] headers = {"Nazwa", "Ilosc", "Cena Brutto", "Cena Netto"};
                raportBuilder.addTableHeaders(headers);
                List<Product> products = document.getOrder().getProducts();
                for (Product product : products) {
                    raportBuilder.addDataToRow(product);
                }
                raportBuilder.endTable();


            }
            raportBuilder.saveFile();

        }
    }



}
