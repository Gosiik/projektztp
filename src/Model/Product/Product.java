package Model.Product;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Product{

    private String name;
    private int amount;
    private double netPrice;
    private double grossPrice;
    private double tax;

    public Product(String name, int amount, double price, double tax) {
        this.name = name;
        this.amount = amount;
        this.tax = tax;
        double x = 1 + tax;
        this.grossPrice = x * price;
        this.netPrice = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getNetPrice() { return netPrice; }

    public void setNetPrice(double netPrice) { this.netPrice = netPrice; }

    public double getGrossPrice() { return grossPrice; }

    public void setGrossPrice(double grossPrice) { this.grossPrice = grossPrice; }

    public String toString(){
        return name + " " + grossPrice;
    }

    public Double getSumGrossPrice(){
        return amount*grossPrice;
    }

    public Double getSumNetPrice(){return amount*netPrice; }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public boolean reduceAmountIfPossible(int value){
        if(amount > value) {
            this.amount -= value;
            return true;
        }
        return false;
    }

}
