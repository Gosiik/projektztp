package Model;

public class TradingPartnerName {

    private String name;

    public TradingPartnerName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString(){
        return name;
    }
}
