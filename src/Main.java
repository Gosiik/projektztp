import DB.DataBase;
import DB.VirtualDataBase;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        Parent root = FXMLLoader.load(getClass().getResource("View/mainView.fxml"));
        primaryStage.setTitle("ZTP");
        primaryStage.setScene(new Scene(root, 632, 436));
        primaryStage.setOnCloseRequest(m -> {
            VirtualDataBase.getInstance().closeConnection();
        });
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
