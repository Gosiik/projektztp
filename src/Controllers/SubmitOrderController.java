package Controllers;

import DB.DataBase;
import DB.VirtualDataBase;
import Model.*;
import Model.Order.IOrder;
import Model.Order.Order;
import Model.Order.OrderWithBigDiscount;
import Model.Order.OrderWithDiscount;
import Model.Product.Product;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.util.List;

public class SubmitOrderController {

    private List<IOrder> orders;
    private List<TradingPartnerName> partners;
    private List<Product> products;

    @FXML
    private ComboBox<TradingPartnerName> listOfPartners;
    @FXML
    private ComboBox<Product> selectedProduct;
    @FXML
    private TableView<Product> tableViewOrder;
    @FXML
    private TextField countOfProduct = new TextField();
    @FXML
    private Label costOfOrder;


    @FXML
    public void initialize() {
        orders = VirtualDataBase.getInstance().getOrders();
        partners = VirtualDataBase.getInstance().getPartners();
        products = VirtualDataBase.getInstance().getProducts();
        addingPartnersToComboBox();
        addingProductsToComboBox();
        initializeTable();
    }

    private void initializeTable() {
        TableColumn<Product, String> name = new TableColumn<>("Nazwa");
        name.setCellValueFactory(new PropertyValueFactory<Product, String>("name"));
        TableColumn<Product, Integer> amount = new TableColumn<>("Liczba");
        amount.setCellValueFactory(new PropertyValueFactory<>("amount"));
        TableColumn<Product, Double> grossPrice = new TableColumn<>("Cena Brutto");
        grossPrice.setCellValueFactory(new PropertyValueFactory<>("grossPrice"));
        TableColumn<Product, Double> netPrice = new TableColumn<>("Cena Netto");
        netPrice.setCellValueFactory(new PropertyValueFactory<>("netPrice"));
        tableViewOrder.getColumns().addAll(name, amount, netPrice, grossPrice);
    }


    private void addingPartnersToComboBox() {
        for (TradingPartnerName tradingPartnerName : partners) {
            listOfPartners.getItems().add(tradingPartnerName);
        }
    }

    private void addingProductsToComboBox() {
        for (Product p : products) {
            selectedProduct.getItems().add(p);
        }

    }


    @FXML
    public void addProductToOrder() {

        Product product = selectedProduct.getSelectionModel().getSelectedItem();

        String nameOfProduct = product.getName();
        String count = countOfProduct.getText();
        int amountOfProduct = Integer.parseInt(count);
        double priceOfProduct = product.getGrossPrice();


        Product newProduct= new Product(nameOfProduct,amountOfProduct,priceOfProduct,product.getTax());

        newProduct.setAmount(amountOfProduct);

        if (VirtualDataBase.getInstance().isPossibleToBuyThisProduct(product, amountOfProduct)){
            tableViewOrder.getItems().add(newProduct);
        } else {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Nie mamy tyle towaru");
            alert.showAndWait();
        }
        costOfOrder.setText(sumOfOrder() + " zł");

    }

    private String sumOfOrder() {
        double sum = 0;
        for (int i = 0; i < tableViewOrder.getItems().size(); i++) {
            sum += tableViewOrder.getItems().get(i).getSumGrossPrice();
        }
        String value = String.valueOf(sum);
        return value;
    }


    @FXML
    public void submitOrder() {

        TradingPartnerName tradingPartnerName = listOfPartners.getSelectionModel().getSelectedItem();
        List<Product> productsInCurrentOrder = tableViewOrder.getItems();
        IOrder currentOrder;
        if (productsInCurrentOrder.size() == 0 || tradingPartnerName == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Dodaj towar oraz wybierz klienta");
            alert.show();
        } else {
            double sum = Double.parseDouble(sumOfOrder());
            if (sum < 1000) {
                currentOrder = new Order(productsInCurrentOrder, tradingPartnerName);
            } else if (sum > 1000 && sum < 10000) {
                currentOrder = new OrderWithDiscount(new Order(productsInCurrentOrder, tradingPartnerName));
            } else {
                currentOrder = new OrderWithBigDiscount(new Order(productsInCurrentOrder, tradingPartnerName));
            }

                VirtualDataBase.getInstance().addOrder(currentOrder);

            }

            Stage stage = (Stage) tableViewOrder.getScene().getWindow();
            stage.close();

        }



}