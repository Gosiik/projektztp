package Controllers;

import DB.DataBase;
import DB.VirtualDataBase;
import Model.Product.Product;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class AddProductController {

    private final String EIGHT = "8%";
    private final String FIFTEEN="15%";
    private final String TWENTYTHREE = "23%";


    /**
     * Start Fields
     */

    @FXML
    private Button existProduct, newProduct;
    @FXML
    private Label chooseTypeLabel;

    /**
     * New product fields
     */

    @FXML
    private Label productNameLabel, productPriceLabel, taxLabel;
    @FXML
    private TextField productNameTextField;
    @FXML
    private TextField productPriceTextField;
    @FXML
    private Text errorAmount;
    @FXML
    private Text errorPrice;
    @FXML
    private ComboBox taxComboBox;

    /**
     * Increase existing product fields
     */

    @FXML
    private Label chooseProductLabel;

    @FXML
    private ComboBox productsComboBox;


    /**
     * Both fields
     */

    @FXML
    private TextField productAmountTextField;

    @FXML
    private Label productAmountLabel;

    @FXML
    private Button addButton;

    /**
     * if strategy = 1 user choosed to add new product
     * if strategy = 2 user choosed to increase exist product amount
     */
    private int strategy = 0;

    private List<Text> errorFields = new ArrayList<>();

    List<Product> products = VirtualDataBase.getInstance().getProducts();

    @FXML
    public void initialize(){

    }

    @FXML
    public void addAction(){
        try {
            if (strategy == 1) {
                String productName = productNameTextField.getText();
                int productAmount = Integer.parseInt(productAmountTextField.getText());
                double productPrice = Double.parseDouble(productPriceTextField.getText());
                String tax = (String) taxComboBox.getSelectionModel().getSelectedItem();
                if (isFieldsCorrectForNewProduct(productName, productAmount, productPrice, tax)) {
                    Product product;
                    switch (tax) {
                        case EIGHT:
                            product = new Product(productName, productAmount, productPrice, 0.08);
                            break;
                        case FIFTEEN:
                            product = new Product(productName, productAmount, productPrice, 0.15);
                            break;
                        case TWENTYTHREE:
                            product = new Product(productName, productAmount, productPrice, 0.23);
                            break;
                        default:
                            product = new Product(productName, productAmount, productPrice, 0.15);
                            break;

                    }
                    VirtualDataBase.getInstance().addProduct(product);
                    Stage stage = (Stage) productNameTextField.getScene().getWindow();
                    stage.close();
                }
            } else {
                String productName = (String) productsComboBox.getSelectionModel().getSelectedItem();
                int productAmount = Integer.parseInt(productAmountTextField.getText());

                if (isFieldsCorrectForExistProduct(productName, productAmount)) {
                    VirtualDataBase.getInstance().increaseProductAmount(productName, productAmount);
                    Stage stage = (Stage) productNameTextField.getScene().getWindow();
                    stage.close();
                }
            }
        }catch (NumberFormatException ex){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("You type incorrect data");
            alert.show();
        }
    }

    public boolean isProductExistInDB(String productName){
        return products.stream().anyMatch(m -> m.getName().equals(productName));
    }

    public boolean isFieldsCorrectForNewProduct(String productName, int productAmount, double productPrice, String tax){
        return !productName.isEmpty() || productAmount > 0 || productPrice > 0 || !tax.isEmpty() || !isProductExistInDB(productName);
    }

    public boolean isFieldsCorrectForExistProduct(String productName, double productAmount){
        return productName != null || productAmount > 0;
    }
/*
    public void isEmpty(String productName, Product product){
        if(products.stream().anyMatch(m -> m.getName().equals(productName))){
            VirtualDataBase.getInstance().increaseProductAmount(productName, product.getAmount());
            Stage stage = (Stage) productNameTextField.getScene().getWindow();
            stage.close();
        }else if(!productName.isEmpty()){
            VirtualDataBase.getInstance().addProduct(product);
            Stage stage = (Stage) productNameTextField.getScene().getWindow();
            stage.close();
        }else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Product with this name exist in database!");
            alert.show();
        }
    }*/

    public void changeVisibiltyForMixFields(){
        existProduct.setVisible(false);
        newProduct.setVisible(false);
        chooseTypeLabel.setVisible(false);
        productAmountTextField.setVisible(true);
        productAmountLabel.setVisible(true);
        addButton.setVisible(true);
    }

    public void changeVisibilityForNewProduct(){


        productNameLabel.setVisible(true);
        productPriceLabel.setVisible(true);
        taxLabel.setVisible(true);
        productNameTextField.setVisible(true);
        productPriceTextField.setVisible(true);
        taxComboBox.setVisible(true);

        changeVisibiltyForMixFields();
    }

    @FXML
    public void newProductAction(){
        strategy = 1;
        changeVisibilityForNewProduct();
        taxComboBox.getItems().addAll(EIGHT,FIFTEEN, TWENTYTHREE);
    }

    public void changeVisibilityForExistProduct(){
        changeVisibiltyForMixFields();
        chooseProductLabel.setVisible(true);
        productsComboBox.setVisible(true);
    }

    @FXML
    public void existingProductAction(){
        strategy = 2;
        changeVisibilityForExistProduct();
        for(Product product : products){
            productsComboBox.getItems().add(product.getName());
        }
    }

}
