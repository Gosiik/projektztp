package Controllers;

import Model.Raport.DirectorRaport;
import Model.Raport.EXCELBuilder;
import Model.Raport.PDFBuilder;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;

public class GenerateRaportController {

    private final String EXCEL = "EXCEL";
    private final String PDF = "PDF";

    @FXML
    private ComboBox typeFileComboBox;

    @FXML
    public void initialize(){
        typeFileComboBox.getItems().addAll(EXCEL,PDF);
    }

    @FXML
    public void generate(){
        String selected = (String) typeFileComboBox.getSelectionModel().getSelectedItem();
        Stage stage = (Stage) typeFileComboBox.getScene().getWindow();

        // Wygeneruje plik excelowski
        if(selected.isEmpty()){

        }
        else if(selected.equals(EXCEL)){
            DirectorRaport report = new DirectorRaport(new EXCELBuilder());
            report.construct();
            stage.close();
        }
        else{ // Wygeneruje plik PDF
            DirectorRaport report = new DirectorRaport(new PDFBuilder());
            report.construct();
            stage.close();
        }

    }

}
