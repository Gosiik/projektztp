package Controllers;

import DB.DataBase;
import DB.VirtualDataBase;
import Model.TradingPartnerName;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.util.List;

public class AddClientController {

    @FXML
    private TextField nameField;

    private List<TradingPartnerName> partners;

    @FXML
    public void initialize(){
        partners = VirtualDataBase.getInstance().getPartners();
    }

    @FXML
    public void add(){
        String name = nameField.getText();

        if(!partners.stream().anyMatch(m -> m.getName().equals(name)) && !name.isEmpty()){
            TradingPartnerName partner = new TradingPartnerName(name);
            VirtualDataBase.getInstance().addPartner(partner);
            Stage stage = (Stage) nameField.getScene().getWindow();
            stage.close();
        }
        else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Partner with this name exist in database!");
            alert.show();
        }

    }
}
