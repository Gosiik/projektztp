package Controllers;

import DB.DataBase;
import DB.VirtualDataBase;
import Model.*;
import Model.Document.Document;
import Model.Order.IOrder;
import Model.Order.Order;
import Model.Product.Product;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;


public class MainController{


    /**
     *
     * Zmienna przetrzymuje informacje jakiego typu dane są przetrzymywane aktualnie w tabeli
     * 1 - Klienci
     * 2 - Dokumenty
     * 3 - Zamówienia
     * 4 - Towary
     *
     */
    private int actuallyData = 0;

    @FXML
    private TextField filterField, givenAmountTextView;

    @FXML
    public void initialize(){

        VirtualDataBase vdb = VirtualDataBase.getInstance();
        // Konfiguracja filtru
        filterField.textProperty().addListener((observable, oldValue, newValue )-> {
            switch (actuallyData){
                case 1:
                    ObservableList<TradingPartnerName> partners = FXCollections.observableArrayList(vdb.getPartners());
                    FilteredList<TradingPartnerName> filteredPartners = new FilteredList<>(partners, p -> true);
                    filteredPartners.setPredicate(partner -> {
                        if(newValue == null || newValue.isEmpty()){
                            return true;
                        }

                        String lowerCaseFilter = newValue.toLowerCase();

                        if(partner.getName().toLowerCase().contains(lowerCaseFilter)){
                            return true;
                        }
                        return false;

                    });
                    SortedList<TradingPartnerName> sortedPartners = new SortedList<>(filteredPartners);
                    sortedPartners.comparatorProperty().bind(tableView.comparatorProperty());
                    partners = FXCollections.observableArrayList(sortedPartners);
                    tableView.setItems(partners);
                    break;
                case 2:
                    ObservableList<Document> documents = FXCollections.observableArrayList(vdb.getDocuments());
                    FilteredList<Document> filteredDocuments = new FilteredList<>(documents, p -> true);
                    filteredDocuments.setPredicate(document -> {
                        if(newValue == null || newValue.isEmpty()){
                            return true;
                        }

                        String lowerCaseFilter = newValue.toLowerCase();

                        if(document.getPartnerName().toLowerCase().contains(lowerCaseFilter)){
                            return true;
                        }else if(document.getDocumentTypeString().toLowerCase().contains(lowerCaseFilter)){
                            return true;
                        }else if(String.valueOf(document.getOrder().getSumNetPrice()).contains(lowerCaseFilter)){
                            return true;
                        }else if(String.valueOf(document.getOrder().getSumGrossPrice()).contains(lowerCaseFilter)){
                            return true;
                        }
                        return false;
                    });
                    SortedList<Document> sortedDocuments = new SortedList<>(filteredDocuments);
                    sortedDocuments.comparatorProperty().bind(tableView.comparatorProperty());
                    documents = FXCollections.observableArrayList(sortedDocuments);
                    tableView.setItems(documents);
                    break;
                case 3:
                    ObservableList<IOrder> orders = FXCollections.observableArrayList(vdb.getOrders());
                    FilteredList<IOrder> filteredOrders = new FilteredList<>(orders, p -> true);
                    filteredOrders.setPredicate(order -> {
                        if(newValue == null || newValue.isEmpty()){
                            return true;
                        }
                        String lowerCaseFilter = newValue.toLowerCase();
                        if(order.getPartner().toLowerCase().contains(lowerCaseFilter)){
                            return true;
                        }else if(String.valueOf(order.getSumGrossPrice()).contains(lowerCaseFilter)){
                            return true;
                        } else if(String.valueOf(order.getSumNetPrice()).contains(lowerCaseFilter)){
                            return true;
                        }
                        return false;
                    });
                    SortedList<IOrder> sortedOrders = new SortedList<>(filteredOrders);
                    sortedOrders.comparatorProperty().bind(tableView.comparatorProperty());
                    orders = FXCollections.observableArrayList(sortedOrders);
                    tableView.setItems(orders);
                    break;
                case 4:
                    ObservableList<Product> products = FXCollections.observableArrayList(vdb.getProducts());
                    FilteredList<Product> filteredProducts = new FilteredList<>(products, p -> true);
                    filteredProducts.setPredicate(product -> {
                        if(newValue == null || newValue.isEmpty()){
                            return true;
                        }
                        String lowerCaseFilter = newValue.toLowerCase();
                        if(product.getName().toLowerCase().contains(lowerCaseFilter)){
                            return true;
                        }else if(String.valueOf(product.getAmount()).contains(lowerCaseFilter)){
                            return true;
                        }else if(String.valueOf(product.getGrossPrice()).contains(lowerCaseFilter)){
                            return true;
                        }else if(String.valueOf(product.getNetPrice()).contains(lowerCaseFilter)){
                            return true;
                        }
                        return false;
                    });
                    SortedList<Product> sortedProducts = new SortedList<>(filteredProducts);
                    sortedProducts.comparatorProperty().bind(tableView.comparatorProperty());
                    products = FXCollections.observableArrayList(sortedProducts);
                    tableView.setItems(products);
                    break;
            }
        });

    }

    private List<Document> documents;


    @FXML
    private TableView tableView;

    /*
    *
    *   Metody związane z zmianą danych w tabeli
    *
     */
    @FXML
    public void clientsAction(){
        cleanTable();
        actuallyData = 1;
        TableColumn<TradingPartnerName, String> partnerNames = new TableColumn<TradingPartnerName, String>("Nazwa partnera");
        partnerNames.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TradingPartnerName, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<TradingPartnerName, String> param) {
                return new ReadOnlyObjectWrapper<>(param.getValue().getName());
            }
        });
        tableView.getColumns().add(partnerNames);

        List<TradingPartnerName> partners = VirtualDataBase.getInstance().getPartners();
        tableView.getItems().addAll(partners);

    }

    @FXML
    public void documentsAction(){
        cleanTable();
        actuallyData = 2;
        TableColumn<Document,String> documentType = new TableColumn<>("Typ dokumentu");
        documentType.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Document, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Document, String> param) {
                return new ReadOnlyObjectWrapper<>(param.getValue().getDocumentTypeString());
            }
        });
        TableColumn<Document,String> partnerName = new TableColumn<>("Klient");
        partnerName.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Document, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Document, String> param) {
                return new ReadOnlyObjectWrapper<>(param.getValue().getPartnerName());
            }
        });
        TableColumn<Document,IOrder> grossPriceSum = new TableColumn<>("Kwota Brutto");
        grossPriceSum.setCellValueFactory(new PropertyValueFactory<>("order"));
        grossPriceSum.setCellFactory(col ->
                new TableCell<Document,IOrder>(){
                    @Override
                    protected void updateItem(IOrder item, boolean empty) {
                        super.updateItem(item, empty);
                        if(empty){
                            setText(null);
                        }
                        else{
                            NumberFormat nf = NumberFormat.getInstance();
                            nf.setMaximumFractionDigits(2);
                            setText(nf.format(item.getSumGrossPrice()));
                        }
                    }
                });
        TableColumn<Document,IOrder> netPriceSum = new TableColumn<>("Kwota Netto");
        netPriceSum.setCellValueFactory(new PropertyValueFactory<>("order"));
        netPriceSum.setCellFactory(col ->
                new TableCell<Document,IOrder>(){
                    @Override
                    protected void updateItem(IOrder item, boolean empty) {
                        super.updateItem(item, empty);
                        if(empty){
                            setText(null);
                        }
                        else{
                            NumberFormat nf = NumberFormat.getInstance();
                            nf.setMaximumFractionDigits(2);
                            setText(nf.format(item.getSumNetPrice()));
                        }
                    }
                });
        TableColumn<Document,String> date = new TableColumn<>("Data");
        date.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Document, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Document, String> param) {
                return new ReadOnlyObjectWrapper<>(param.getValue().getDateString());
            }
        });

        tableView.getColumns().addAll(documentType,partnerName, grossPriceSum,netPriceSum, date);

        documents = VirtualDataBase.getInstance().getDocuments();
        tableView.getItems().addAll(documents);

    }

    @FXML
    public void ordersAction(){
        cleanTable();
        actuallyData = 3;
        TableColumn<IOrder,String> partnerName = new TableColumn<>("Nazwa partnera");
        partnerName.setCellValueFactory(new PropertyValueFactory<>("partner"));
        TableColumn<IOrder,Double> sumNetPrice = new TableColumn<>("Cena netto");
        sumNetPrice.setCellValueFactory(new PropertyValueFactory<>("sumNetPrice"));
        sumNetPrice.setCellFactory(col ->
            new TableCell<IOrder,Double>(){
                @Override
                protected void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    if(empty){
                        setText(null);
                    }
                    else{
                        NumberFormat nf = NumberFormat.getInstance();
                        nf.setMaximumFractionDigits(2);
                        setText(nf.format(item));
                    }
                }
            });
        TableColumn<IOrder,Double> sumGrossPrice = new TableColumn<>("Cena brutto");
        sumGrossPrice.setCellValueFactory(new PropertyValueFactory<>("sumGrossPrice"));
        sumGrossPrice.setCellFactory(col ->
                new TableCell<IOrder,Double>(){
                    @Override
                    protected void updateItem(Double item, boolean empty) {
                        super.updateItem(item, empty);
                        if(empty){
                            setText(null);
                        }
                        else{
                            NumberFormat nf = NumberFormat.getInstance();
                            nf.setMaximumFractionDigits(2);
                            setText(nf.format(item));
                        }
                    }
                });
        TableColumn<IOrder, String> info = new TableColumn<>("");
        info.setCellFactory(col -> {
            TableCell cell = new TableCell<IOrder, String>(){
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if(empty){
                        setText(null);
                    }
                    else{
                        setText("Zobacz produkty");
                    }
                }
            };
            cell.setOnMouseClicked(event -> {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                Order order = (Order) tableView.getSelectionModel().getSelectedItem();
                alert.setHeaderText("Produkty zamówienia nr: " + order.getOrderID());
                StringBuilder contentText = new StringBuilder("");
                for(Product product : order.getProducts()){
                    NumberFormat nf = NumberFormat.getInstance();
                    nf.setMaximumFractionDigits(2);
                    String netPrice = nf.format(product.getSumNetPrice());
                    String grossPrice = nf.format(product.getSumGrossPrice());
                    contentText.append("Nazwa: " + product.getName() + " Ilość: " + product.getAmount() + " Cena netto: " +netPrice + " Cena brutto: " + grossPrice + "\n");
                }
                alert.setContentText(contentText.toString());
                alert.showAndWait();

            });
            return cell;
        });

        tableView.getColumns().addAll(partnerName, sumGrossPrice, sumNetPrice, info);

        tableView.getItems().addAll(VirtualDataBase.getInstance().getOrders());
    }

    @FXML
    public void productsAction(){
        cleanTable();
        givenAmountTextView.setVisible(true);
        actuallyData = 4;
        TableColumn<Product, String> name = new TableColumn<>("Nazwa");
        name.setCellValueFactory(new PropertyValueFactory<Product,String>("name"));
        TableColumn<Product, Integer> amount = new TableColumn<>("Liczba");
        amount.setCellValueFactory(new PropertyValueFactory<>("amount"));
        TableColumn<Product, Double> netPrice = new TableColumn<>("Cena Netto");
        netPrice.setCellValueFactory(new PropertyValueFactory<>("netPrice"));
        netPrice.setCellFactory(col ->
                new TableCell<Product, Double>(){
                    @Override
                    protected void updateItem(Double item, boolean empty) {
                        super.updateItem(item, empty);
                        if(empty){
                            setText(null);
                        }
                        else{
                            NumberFormat nf = NumberFormat.getInstance();
                            nf.setMaximumFractionDigits(2);
                            setText(nf.format(item));
                        }
                    }
                });
        TableColumn<Product, Double> grossPrice = new TableColumn<>("Cena Brutto");
        grossPrice.setCellValueFactory(new PropertyValueFactory<>("grossPrice"));
        grossPrice.setCellFactory(col ->
                new TableCell<Product, Double>(){
                    @Override
                    protected void updateItem(Double item, boolean empty) {
                        super.updateItem(item, empty);
                        if(empty){
                            setText(null);
                        }
                        else{
                            NumberFormat nf = NumberFormat.getInstance();
                            nf.setMaximumFractionDigits(2);
                            setText(nf.format(item));
                        }
                    }
                });

        tableView.getColumns().addAll(name, amount, netPrice,grossPrice);

        List<Product> products = VirtualDataBase.getInstance().getProducts();
        tableView.getItems().addAll(products);

    }

    public void cleanTable(){
        filterField.setText("");
        givenAmountTextView.setVisible(false);
        tableView.getColumns().removeAll(tableView.getColumns());
        tableView.getItems().removeAll(tableView.getItems());

    }

    /*
    *
    *   Metody związane z dodawaniem danych
    *
     */

    @FXML
    public void addClient(){
        try {
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("View/addClientView.fxml"));
            Stage addClientStage = new Stage();
            addClientStage.setTitle("Dodawanie klienta");
            addClientStage.setScene(new Scene(root, 200, 200));
            addClientStage.show();
        }catch (IOException ex){
            System.out.println("There was problem with loading new Window\n");
            ex.printStackTrace();
        }
    }

    @FXML
    public void addDocument(){
        try {
            IOrder order = (IOrder) tableView.getSelectionModel().getSelectedItem();
            if(order != null) {
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("View/addDocumentView.fxml"));

                    Parent root = loader.load();
                    Stage addClientStage = new Stage();
                    addClientStage.setTitle("Dodawanie dokumentu");
                    addClientStage.setScene(new Scene(root, 200, 300));

                    AddDocumentController controller = loader.getController();
                    controller.initOrder(order);

                    addClientStage.show();

                } catch (IOException ex) {
                    System.out.println("There was problem with loading new Window\n");
                    ex.printStackTrace();
                }
            }else{
                throw new ClassCastException();
            }

        }catch (ClassCastException ex){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Musisz wybrać zamówienie aby utworzyć dla niego dokument!");
            alert.show();
        }
    }

    @FXML
    public void addProduct(){
        try {
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("View/addProductView.fxml"));
            Stage addProductStage = new Stage();
            addProductStage.setTitle("Dodawanie produktu");
            addProductStage.setScene(new Scene(root, 200, 300));
            addProductStage.show();
        }catch (IOException ex){
            System.out.println("There was problem with loading new Window\n");
            ex.printStackTrace();
        }
    }

    @FXML
    public void submitOrder() {
        try {
            Parent root = (Parent)FXMLLoader.load(this.getClass().getClassLoader().getResource("View/submitOrderView.fxml"));
            Stage submitOrderStage = new Stage();
            submitOrderStage.setTitle("Submit Order");
            submitOrderStage.setScene(new Scene(root, 475.0D, 329.0D));
            submitOrderStage.show();
        } catch (IOException var3) {
            System.out.println("There was problem with loading new Window\n");
            var3.printStackTrace();
        }

    }

    @FXML
    public void reduceProductAmountAction(){
        try{
            Product product = (Product) tableView.getSelectionModel().getSelectedItem();
            if(product == null){
                throw new ClassCastException();
            }
            if(!givenAmountTextView.getText().isEmpty()) {
                int value = Integer.valueOf(givenAmountTextView.getText());
                boolean reduced = VirtualDataBase.getInstance().isPossibleToBuyThisProduct(product, value);
                if(reduced == false){
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setContentText("Nie ma tyle towaru w magazynie!");
                    alert.show();
                }
                else{
                    VirtualDataBase.getInstance().reduceProductAmount(product);
                }
                tableView.refresh();
            }
            else {
                throw new NumberFormatException();
            }

        }catch (ClassCastException ex){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Musisz wybrać towar do wydania");
            alert.show();
        }catch (NumberFormatException ex){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Wpisane dane są błędne");
            alert.show();
        }
    }

    @FXML
    public void generateRaport(){
        try {
            Parent root = (Parent)FXMLLoader.load(this.getClass().getClassLoader().getResource("View/generateRaportView.fxml"));
            Stage submitOrderStage = new Stage();
            submitOrderStage.setTitle("Generate Raport");
            submitOrderStage.setScene(new Scene(root, 200, 150));
            submitOrderStage.show();
        } catch (IOException ex) {
            System.out.println("There was problem with loading new Window\n");
            ex.printStackTrace();
        }
    }


}
