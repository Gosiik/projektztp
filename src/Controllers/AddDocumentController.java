package Controllers;

import DB.DataBase;
import DB.VirtualDataBase;
import Model.*;
import Model.Document.BillStrategy;
import Model.Document.Document;
import Model.Document.ReceiptStrategy;
import Model.Order.IOrder;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.time.LocalDate;
import java.util.List;

public class AddDocumentController {

    private final String BILL = "Faktura";
    private final String RECEIPT = "Paragon";

    private final String NIPREGEX = "\\d{3}[-]\\d{3}[-]\\d{2}[-]\\d{2}";

    private IOrder order;

    public void initOrder(IOrder order){
        this.order = order;
    }

    @FXML
    private ComboBox documentTypeComboBox;
    @FXML
    private TextField adressTextField,nipTextField;

    private List<TradingPartnerName> partners = VirtualDataBase.getInstance().getPartners();

    @FXML
    public void initialize(){
        documentTypeComboBox.getItems().addAll(BILL,RECEIPT);

        documentTypeComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if(newValue.equals(RECEIPT)){
                    changeFields(true);
                }
                else{
                    changeFields(false);
                }
            }
        });
    }

    public void changeFields(boolean value){
        adressTextField.setDisable(value);
        nipTextField.setDisable(value);
    }

    @FXML
    public void addAction(){
        String selectedItem = (String) documentTypeComboBox.getSelectionModel().getSelectedItem();
        Stage stage = (Stage) documentTypeComboBox.getScene().getWindow();
        if(selectedItem.equals(BILL)){
            if(isFieldsCorrect()){
                Document document = new Document(order, new BillStrategy(order.getPartnerObject(),adressTextField.getText(),nipTextField.getText()), LocalDate.now());
                VirtualDataBase.getInstance().addBillDocument(document);
                stage.close();
            }
            else{
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Wprowadzone dane są błędne");
                alert.show();
            }
        }
        else{
            // save
            Document document = new Document(order, new ReceiptStrategy(), LocalDate.now());
            VirtualDataBase.getInstance().addReceiptDocument(document);
            stage.close();
        }

        //DataBase.getInstance().deleteOrder(order);

    }

    public boolean isFieldsCorrect(){
        if(
                nipTextField.getText().matches(NIPREGEX) &&
                !adressTextField.getText().isEmpty()
        ){
            return true;
        }
        return false;
    }
}
