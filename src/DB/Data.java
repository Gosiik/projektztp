package DB;

import Model.Document.BillStrategy;
import Model.Document.Document;
import Model.Document.DocumentStrategy;
import Model.Document.ReceiptStrategy;
import Model.Order.IOrder;
import Model.Order.Order;
import Model.Order.OrderWithBigDiscount;
import Model.Order.OrderWithDiscount;
import Model.Product.Product;
import Model.TradingPartnerName;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public interface Data {

    public List<TradingPartnerName> getPartners();
    public void addPartner(TradingPartnerName partner);
    List<IOrder> getOrders();
    void addOrder(IOrder order);
    List<Product> getProducts();
    void addProduct(Product product);
    List<Document> getDocuments();
    void addBillDocument(Document document);
    void addReceiptDocument(Document document);
    void closeConnection();
    void reduceProductAmount(Product product);
    void increaseProductAmount(String productName, int value);

}
