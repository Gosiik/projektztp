package DB;

import Model.*;
import Model.Document.BillStrategy;
import Model.Document.Document;
import Model.Document.DocumentStrategy;
import Model.Document.ReceiptStrategy;
import Model.Order.IOrder;
import Model.Order.Order;
import Model.Order.OrderWithBigDiscount;
import Model.Order.OrderWithDiscount;
import Model.Product.Product;

import java.sql.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DataBase implements Data{

    private final String getPartnersQuery = "Select * From Partners";
    private final String getOrdersQuery = "SELECT * FROM ORDERS o WHERE o.ID_ORDER <> ANY(SELECT ID_ORDER FROM DOCUMENTS)";
    private final String getDocumentsQuery = "Select * From Documents";
    private Connection connection;

    public DataBase(){

        final String driver = "oracle.jdbc.driver.OracleDriver";
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/orcl.home", "Gerard","podenter127");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(getPartnersQuery);

            while (resultSet.next()){
                System.out.println(resultSet.getString("NAME"));
            }
            statement.close();
            //connection.close();
        }catch (ClassNotFoundException ex){
            ex.printStackTrace();
        }catch (SQLException ex){
            ex.printStackTrace();
        }

    }


    public List<TradingPartnerName> getPartners() {
        List<TradingPartnerName> partners = new LinkedList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(getPartnersQuery);

            while (resultSet.next()) {
                String name = resultSet.getString("NAME");
                partners.add(new TradingPartnerName(name));
            }

            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return partners;
    }

    public void addPartner(TradingPartnerName partner){
        //partners.add(partner);
        try {
            Statement statement = connection.createStatement();
            String query = "INSERT INTO PARTNERS(NAME) VALUES('"+partner.getName()+"')";
            statement.executeQuery(query);

            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    public TradingPartnerName getPartnerByID(String id){
        TradingPartnerName partner = null;
        try {
            Statement statement = connection.createStatement();
            ResultSet partnerResult = statement.executeQuery("SELECT * FROM PARTNERS WHERE ID_PARTNER = " + id);

            while (partnerResult.next()) {
                String partnerName = partnerResult.getString("NAME");
                partner = new TradingPartnerName(partnerName);
            }
            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return partner;
    }

    public IOrder setDiscount(List<Product> products, TradingPartnerName partner){
        Double sum = 0.0;
        IOrder order;
        for(Product product:products){
            sum += product.getSumGrossPrice();
        }
        if (sum < 1000) {
            order = new Order(products, partner);
        } else if (sum > 1000 && sum < 10000) {
            order = new OrderWithDiscount(new Order(products, partner));
        } else {
            order = new OrderWithBigDiscount(new Order(products, partner));
        }

        return order;
    }

    public boolean checkIfAnyDocumentExist(){
        List<Document> documents = getDocuments();
        if(documents.size() != 0){
            return true;
        }
        return false;
    }

    public List<IOrder> getOrders() {
        List<IOrder> orders = new LinkedList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet;
            if(checkIfAnyDocumentExist()) {
                resultSet = statement.executeQuery(getOrdersQuery);
            }else{
                String query = "SELECT * FROM ORDERS";
                resultSet = statement.executeQuery(query);
            }
            while (resultSet.next()) {
                String idOrder = resultSet.getString("ID_ORDER");
                String idPartner = resultSet.getString("ID_PARTNER");

                TradingPartnerName partner = getPartnerByID(idPartner);
                List<Product> products = getProductsByOrderId(idOrder);
                IOrder order = setDiscount(products,partner);
                order.setOrderID(Integer.valueOf(idOrder));
                orders.add(order);

            }

            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return orders;
    }

    private List<Product> getProductsByOrderId(String idOrder) {
        List<Product> products = new LinkedList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select * FROM PRODUCTS WHERE ID_ORDER = " + idOrder);

            while (resultSet.next()) {
                String name = resultSet.getString("NAME");
                int amount = resultSet.getInt("AMOUNT");
                Double price = resultSet.getDouble("PRICE");
                Double tax = resultSet.getDouble("TAX");
                Product product = new Product(name,amount,price,tax);
                products.add(product);
            }

            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return products;
    }

    public int getIdPartnerByName(String name){
        Integer id = null;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select * FROM PARTNERS WHERE NAME = '" + name + "'");

            while (resultSet.next()) {
                id = resultSet.getInt("ID_PARTNER");
            }

            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return id;

    }

    public void addOrder(IOrder order){
        try {
            Statement statement = connection.createStatement();

            int idPartner = getIdPartnerByName(order.getPartner());
            String insertOrderQuery = "INSERT INTO ORDERS(ID_PARTNER) VALUES("+idPartner+")";
            statement.executeQuery(insertOrderQuery);
            String getNewOrderIDQuery = "SELECT MAX(ID_ORDER) FROM ORDERS";
            ResultSet resultSet = statement.executeQuery(getNewOrderIDQuery);
            if(resultSet.next()){
                int orderID = resultSet.getInt(1);
                for(Product product: order.getProducts()){
                    addProductWithOrderID(product, orderID);
                    reduceProductAmount(product);
                }
            }

            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    public List<Product> getProducts() {
        List<Product> products = new LinkedList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select * FROM PRODUCTS WHERE ID_ORDER IS NULL");

            while (resultSet.next()) {
                String name = resultSet.getString("NAME");
                int amount = resultSet.getInt("AMOUNT");
                Double price = resultSet.getDouble("PRICE");
                Double tax = resultSet.getDouble("TAX");
                Product product = new Product(name, amount, price, tax);
                products.add(product);
            }

            statement.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return products;
    }

    public void addProduct(Product product){
        try {
            Statement statement = connection.createStatement();
            String name = product.getName();
            int amount = product.getAmount();
            Double price = product.getNetPrice();
            Double tax = product.getTax();

            String query =
                    "INSERT INTO PRODUCTS(NAME,AMOUNT,PRICE,TAX) VALUES('"
                            + product.getName()+ "', "
                            + amount + ","
                            + price + ","
                            + tax
                            + ")";
            statement.executeQuery(query);

            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    public void addProductWithOrderID(Product product, int orderID){
        try {
            Statement statement = connection.createStatement();
            String name = product.getName();
            int amount = product.getAmount();
            Double price = product.getNetPrice();
            Double tax = product.getTax();

            String query =
                    "INSERT INTO PRODUCTS(NAME,AMOUNT,PRICE,TAX,ID_ORDER) VALUES('"
                            + product.getName()+ "', "
                            + amount + ","
                            + price + ","
                            + tax + ","
                            + orderID + ")";
            statement.executeQuery(query);

            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }


    public IOrder getOrderByID(int id){
        IOrder order = null;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(getOrdersQuery);

            while (resultSet.next()) {
                String idOrder = resultSet.getString("ID_ORDER");
                String idPartner = resultSet.getString("ID_PARTNER");

                TradingPartnerName partner = getPartnerByID(idPartner);
                List<Product> products = getProductsByOrderId(idOrder);
                order = new Order(products,partner);
            }

            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return order;
    }

    public List<Document> getDocuments() {
        List<Document> documents = new LinkedList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(getDocumentsQuery);

            while (resultSet.next()) {
                int documentID =  resultSet.getInt("ID_DOCUMENT");
                int orderID = resultSet.getInt("ID_ORDER");
                int billID = resultSet.getInt("ID_BILL");
                Date date = resultSet.getDate("CREATE_DATE");
                LocalDate localDate = date.toLocalDate();
                IOrder order = getOrderByID(orderID);
                Document document = null;
                if(billID == 0){
                    document = new Document(order, new ReceiptStrategy(), localDate);
                }
                else{
                    BillStrategy billStrategy = getBillByDocumentID(documentID, order.getPartnerObject());
                    document = new Document(order, billStrategy, localDate);
                }

                documents.add(document);
            }

            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return documents;
    }

    private BillStrategy getBillByDocumentID(int documentID, TradingPartnerName partner) {
        BillStrategy bill = null;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM BILL WHERE ID_DOCUMENT = " + documentID);

            while (resultSet.next()) {
                String nip = resultSet.getString("NIP");
                String adress = resultSet.getString("ADRESS");

                bill = new BillStrategy(partner,adress,nip);
            }

            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return bill;
    }

    public void addBill(DocumentStrategy bill){
        String nip = bill.getNip();
        String address = bill.getAddress();
        try {
            Statement statement = connection.createStatement();

            String query =  "INSERT INTO BILL(NIP,ADDRESS) VALUES( " +
                    nip + "'," +
                    address + "')";


            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    public void addBillDocument(Document document){
        try {
            Statement statement = connection.createStatement();

            addBill(document.getDocumentStrategy());
            String getNewOrderIDQuery = "SELECT MAX(ID_BILL) FROM BILL";
            ResultSet resultSet = statement.executeQuery(getNewOrderIDQuery);
            if(resultSet.next()) {
                int billID = resultSet.getInt(1);
                Date date = Date.valueOf(document.getDate());

                String query =  "INSERT INTO DOCUMENTS(ID_ORDER,ID_BILL,CREATE_DATE) VALUES( " +
                        document.getOrder().getOrderID() + "," +
                        billID + "," +
                        date + ")";

                statement.executeQuery(query);
            }

            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    public void addReceiptDocument(Document document) {
        try {
            Statement statement = connection.createStatement();

            Date date = Date.valueOf(document.getDate());

            String query =  "INSERT INTO DOCUMENTS(ID_ORDER,ID_BILL,CREATE_DATE) VALUES( " +
                    document.getOrder().getOrderID() + "," +
                    "null,'" +
                    date.toString() + "')";

            statement.executeQuery(query);

            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void reduceProductAmount(Product product){
        String name = product.getName();
        try {
            Statement statement = connection.createStatement();

            String updateQuery = "UPDATE PRODUCTS SET AMOUNT = AMOUNT - " + product.getAmount() +" WHERE NAME = '" +product.getName() + "' AND ID_ORDER IS NULL";
            ResultSet resultSet = statement.executeQuery(updateQuery);
            statement.close();

            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }


    }

    @Override
    public void increaseProductAmount(String productName, int value) {
        try {
            Statement statement = connection.createStatement();

            String updateQuery = "UPDATE PRODUCTS SET AMOUNT = AMOUNT + " + value +" WHERE NAME = '" +productName + "' AND ID_ORDER IS NULL";
            ResultSet resultSet = statement.executeQuery(updateQuery);
            statement.close();

            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    public void closeConnection(){
        try {
            connection.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }
}
