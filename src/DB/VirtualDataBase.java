package DB;

import Model.Document.Document;
import Model.Order.IOrder;
import Model.Product.Product;
import Model.TradingPartnerName;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class VirtualDataBase implements Data {
    private List<IOrder> orders;
    private List<Product> products;
    private List<Document> documents;
    private List<TradingPartnerName> partners;
    private DataBase db = new DataBase();

    private static VirtualDataBase INSTANCE = new VirtualDataBase();
    public static VirtualDataBase getInstance(){ return INSTANCE;}

    private VirtualDataBase() { }

    @Override
    public List<TradingPartnerName> getPartners() {
        if(partners == null){
            partners = db.getPartners();
        }
        return partners;

    }

    @Override
    public void addPartner(TradingPartnerName partner) {
        if(partners == null){
            partners = new LinkedList<>();
        }
        partners.add(partner);
        db.addPartner(partner);
    }

    @Override
    public List<IOrder> getOrders() {
        if(orders == null){
            orders = db.getOrders();
        }
        return orders;
    }

    @Override
    public void addOrder(IOrder order){
        if(orders == null){
            orders = new LinkedList<>();
        }
        orders.add(order);
        db.addOrder(order);
    }

    @Override
    public List<Product> getProducts() {
        if(products == null){
            products = db.getProducts();
        }
        return products;
    }

    @Override
    public void addProduct(Product product) {
        if(products == null){
            products = new LinkedList<>();
        }
        products.add(product);
        db.addProduct(product);
    }

    @Override
    public List<Document> getDocuments() {
        if(documents == null){
            documents = db.getDocuments();
        }
        return documents;
    }

    @Override
    public void addBillDocument(Document document) {
        if(documents == null){
            documents = new LinkedList<>();
        }
        documents.add(document);
        db.addBillDocument(document);
    }

    @Override
    public void addReceiptDocument(Document document) {
        if(documents == null){
            documents = new LinkedList<>();
        }
        documents.add(document);
        db.addReceiptDocument(document);
    }

    @Override
    public void increaseProductAmount(String productName, int value){
        Product productInMagazine = products.stream()
                .filter(m -> m.getName().equals(productName))
                .findAny()
                .orElse(null);
        int newAmount = productInMagazine.getAmount() + value;
        productInMagazine.setAmount(newAmount);
        db.increaseProductAmount(productName,value);
    }

    @Override
    public void reduceProductAmount(Product product){
        Product productInMagazine = products.stream()
                .filter(m -> m.getName().equals(product.getName()))
                .findAny()
                .orElse(null);

        int newAumount = productInMagazine.getAmount() - product.getAmount();
        productInMagazine.setAmount(newAumount);

        db.reduceProductAmount(product);
    }


    public boolean isPossibleToBuyThisProduct(Product product, int value){
        Product productInMagazine = products.stream()
                .filter(m -> m.getName().equals(product.getName()))
                .findAny()
                .orElse(null);

        int newAumount = productInMagazine.getAmount() - value;
        if(newAumount > 0){
            return true;
        }
        return false;
    }

    @Override
    public void closeConnection() {
        db.closeConnection();
    }




}
